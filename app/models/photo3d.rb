class Photo3d < ActiveRecord::Base
  has_attached_file :photo3d, :styles => { :webdoor => "230x230#", :thumb => "80x80#", :list => "50x50#", :big => "1024", :normal => "300" }
  belongs_to :product
  validates_presence_of :product_id
  
  def image
    photo3d.url(:medium)
  end
end
