class Search < ActiveRecord::Base
  has_no_table
  
  column :content, :string #used in simple_find
  column :simple, :boolean
  column :page, :integer

  def find
    if simple
      simple_find
    end
  end


  def simple_find
    like_search = "%#{content}%"
    
    
    Product.actives.paginate :conditions => ['products.name like ? or categories.name like ?', like_search, like_search],
      :joins => :categories,
      :group => "products.id",
      :per_page => 20, :page => self.page || 1
  end
end
