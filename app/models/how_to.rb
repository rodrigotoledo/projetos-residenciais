class HowTo < ActiveRecord::Base
  validates_presence_of :question, :reply
  validates_uniqueness_of :question
  default_scope :order => "question"
end
