class Role < ActiveRecord::Base
  acts_as_authorization_role
  
  DEFINITIONS = ["Macroprocesos","Segurança","Cadastro","Metas estratégicas","Virada ano","Ações estratégicas",
    "Execução","Subações","Acompanhamento"]
end
