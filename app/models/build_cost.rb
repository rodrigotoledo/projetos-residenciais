class BuildCost < ActiveRecord::Base
  validates_presence_of :product_id, :cost_id, :price, :state
  validates_uniqueness_of :product_id, :scope => [:product_id, :cost_id]
  belongs_to :product
  belongs_to :cost
end
