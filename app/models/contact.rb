class Contact < ActiveRecord::Base
  has_no_table
  column :name, :string
  column :email, :string
  column :subject, :string
  column :message, :string
  validates_presence_of :name, :email, :subject, :message
  
  
  def send_mail
    return false unless self.valid?
    
    Mail.deliver_contact(name,email,subject,message)
  end
end
