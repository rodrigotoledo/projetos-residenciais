class News < ActiveRecord::Base
  default_scope :order => "id desc"
  validates_presence_of :title, :resume, :content
  has_many :news_photos
  accepts_nested_attributes_for :news_photos, :allow_destroy => true
end
