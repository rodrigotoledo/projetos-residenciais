class NewsPhoto < ActiveRecord::Base
  has_attached_file :photo, :styles => { :normal => "200x200#", :small => "150", :list => "50x50#"}, :default_style => :normal
end
