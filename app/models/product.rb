class Product < ActiveRecord::Base
  has_and_belongs_to_many :categories
  validates_presence_of :name, :description, :category_ids, :m2, :mhouse, :msuggestion, :product_values
  validates_uniqueness_of :name
  validates_associated :categories
  has_many :photo3ds, :dependent => :destroy
  has_many :product_attributes, :dependent => :destroy
  has_many :clear_attributes, :class_name => "ProductAttribute", :conditions => ["product_attributes.description <> ?",""]
  has_many :product_values, :dependent => :destroy, :order => "description"
  has_many :product_values_optionals, :class_name => "ProductValue", :order => "description", :conditions => {:leader_price => false}
  has_many :photos, :dependent => :destroy
  has_many :comments, :dependent => :destroy
  has_many :comments_awnsers, :class_name => "Comment", :conditions => {:comment_id => nil, :active => true}
  has_one :photo
  has_one :price, :class_name => "ProductValue", :order => "leader_price DESC, description"
  
  named_scope :actives, :conditions => {:is_active => true}, :order => "products.updated_at DESC"
  
  
  def public_id
    [id].concat(name.split(" ")).collect{|t| t.to_s.downcase.strip}.compact.join("-").gsub(/[,|.|\/|?]/,'')
  end
  
  def image(size = :webdoor)
    photo.photo.url(size)
  end
  
  def image_thumb
    image(:thumb)
  end
  
  def to_s
    name
  end

  def photo_attributes=(photo_attributes)
    photo_attributes.each do |attributes|
      photos.build(attributes)
    end
  end
  
  def photo3d_attributes=(photo_attributes)
    photo_attributes.each do |attributes|
      photo3ds.build(attributes)
    end
  end
  
  def product_attribute_attributes=(product_attribute_attributes)
    product_attribute_attributes.each do |attributes|
      product_attributes.build(attributes)
    end
  end
  def product_value_attributes=(product_value_attributes)
    product_value_attributes.each do |attributes|
      product_values.build(attributes)
    end
  end
  
  def set_dependencies
    
    product_type_ids = product_values.collect{|t| t.product_type_id}
    product_type_ids = [0] if product_type_ids.size == 0
    ProductType.all(:conditions => ["id not in(?)",product_type_ids]).each do |p|
      product_values.build :product_type_id => p.id
    end
    
    
    attribute_ids = product_attributes.collect{|t| t.attribute_id}
    attribute_ids = [0] if attribute_ids.size == 0
    Attribute.all(:conditions => ["id not in(?)",attribute_ids]).each do |a|
      product_attributes.build :attribute_id => a.id
    end
  end
end
