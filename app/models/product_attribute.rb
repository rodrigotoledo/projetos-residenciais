class ProductAttribute < ActiveRecord::Base
  belongs_to :product
  belongs_to :attribute
  
  def self.admin_list(product_id,page)
    page ||= 1
    if product_id.blank?
      ProductAttribute.paginate :per_page => 20, :page => page, :order => "product_id,attribute_id"
    else
      ProductAttribute.paginate :per_page => 100, :page => page, :order => "product_id,attribute_id", :conditions => {:product_id => product_id}
    end
  end
end
