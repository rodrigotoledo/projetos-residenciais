class NumeroVisitante < ActiveRecord::Base
  def self.registrar(ip)
    if NumeroVisitante.exists?(["ip = ?  AND created_at BETWEEN ? AND ?",ip,Time.now.beginning_of_day,Time.now.end_of_day])
      return
    end
    NumeroVisitante.create :ip => ip
    
  end
end
