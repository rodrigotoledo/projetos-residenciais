module CarrinhoHelper
  def pagseguro_form2(order, options={})
    options = {
      :submit => "Pagar com PagSeguro",
      :client => {:name => "",
        :cep => "",
        :address => "",
        :number => "",
        :address2 => "",
        }
    }.merge(options)

    render :partial => "ps_form", :locals => {:options => options, :order => order}
  end
end
