module CartItemsHelper
  
  def options_for_quantity(quantity)
    (1...(quantity+3)).to_a
  end
end
