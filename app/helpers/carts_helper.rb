module CartsHelper
  def status_options
    [
      'Pendente','Em andamento','Aguardando resposta do cliente',
      'Concluído com sucesso', 'Cancelado'
    ]
  end
end
