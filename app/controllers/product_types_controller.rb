class ProductTypesController < AdminController
  def index
    @product_types = ProductType.paginate :per_page => 20, :page => params[:page]
  end
  
  def new
    @product_type = ProductType.new
  end
  
  def create
    @product_type = ProductType.new(params[:product_type])
    if @product_type.save
      flash[:notice] = "Successfully created product type."
      redirect_to product_types_url
    else
      render :action => 'new'
    end
  end
  
  def edit
    @product_type = ProductType.find(params[:id])
  end
  
  def update
    @product_type = ProductType.find(params[:id])
    if @product_type.update_attributes(params[:product_type])
      flash[:notice] = "Successfully updated product type."
      redirect_to product_types_url
    else
      render :action => 'edit'
    end
  end
end
