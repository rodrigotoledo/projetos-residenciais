class AdminController < ApplicationController
  access_control do
    allow "Admin"
  end
  layout "admin"
end