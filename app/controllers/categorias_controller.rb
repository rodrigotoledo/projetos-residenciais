class CategoriasController < ApplicationController
  def index
  end

  def show
    unless Category.exists?(params[:id])
      redirect_to categorias_path
      return
    end
    
    @category = Category.find(params[:id])
    @products = @category.products.actives.paginate :per_page => 25, :page => params[:page]
  end

end
