class NoticiasController < ApplicationController
  def index
    @news = News.all
    redirect_to root_path if @news.size == 0
  end

  def show
    @news = News.find params[:id]
  end

end
