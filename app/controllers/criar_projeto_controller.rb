class CriarProjetoController < ApplicationController
  def index
    @project_modification = ProjectModification.new
    @project_modification.type_modification = ProjectModification::PROJETO_A_CRIAR
  end

  def criar
    @project_modification = ProjectModification.new(params[:project_modification])
    
    unless @project_modification.save
      render :action => :index
    end
  end

end
