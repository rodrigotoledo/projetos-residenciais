class AttributesController < AdminController
  
  def index
    @attributes = Attribute.all
  end
  
  def new
    @attribute = Attribute.new
  end
  
  def create
    @attribute = Attribute.new(params[:attribute])
    if @attribute.save
      flash[:notice] = "Successfully created attribute."
      redirect_to attributes_url
    else
      render :action => :new
    end
  end
  
  def edit
    @attribute = Attribute.find(params[:id])
  end
  
  def update
    @attribute = Attribute.find(params[:id])
    if @attribute.update_attributes(params[:attribute])
      flash[:notice] = "Successfully updated attribute."
      redirect_to attributes_url
    else
      render :action => :edit
    end
  end
  
  def destroy
    @attribute = Attribute.find(params[:id])
    @attribute.destroy
    flash[:notice] = "Successfully destroyed attribute."
    redirect_to attributes_url
  end

end
