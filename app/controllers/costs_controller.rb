class CostsController < AdminController
  def index
    @costs = Cost.paginate :page => params[:page]
  end
  
  def show
    @cost = Cost.find(params[:id])
  end
  
  def new
    @cost = Cost.new
  end
  
  def create
    @cost = Cost.new(params[:cost])
    if @cost.save
      flash[:notice] = "Custo criado com sucessso."
      redirect_to @cost
    else
      render :action => 'new'
    end
  end
  
  def edit
    @cost = Cost.find(params[:id])
  end
  
  def update
    @cost = Cost.find(params[:id])
    if @cost.update_attributes(params[:cost])
      flash[:notice] = "Custo atualizado com sucesso."
      redirect_to @cost
    else
      render :action => 'edit'
    end
  end
  
  def destroy
    @cost = Cost.find(params[:id])
    @cost.destroy
    flash[:notice] = "Custo removido com sucesso."
    redirect_to costs_url
  end
end
