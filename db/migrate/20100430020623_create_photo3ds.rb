class CreatePhoto3ds < ActiveRecord::Migration
  def self.up
    create_table :photo3ds do |t|
      t.references :product
      t.string :photo3d_file_name
      t.integer :photo3d_file_size
      t.datetime :photo3d_updated_at
      t.string :photo3d_content_type
      t.timestamps
    end
  end
  
  def self.down
    drop_table :photo3ds
  end
end
