class CreatePayments < ActiveRecord::Migration
  def self.up
    create_table :payments do |t|
      t.references :cart
      t.string :email
      t.string :email2
      t.string :name
      t.text :observations
      t.string :phone
      t.string :celphone
      t.string :address
      t.string :state
      t.string :complement
      t.string :status
      t.string :service_id
      t.string :payment_method
      t.timestamps
    end
  end
  
  def self.down
    drop_table :payments
  end
end
