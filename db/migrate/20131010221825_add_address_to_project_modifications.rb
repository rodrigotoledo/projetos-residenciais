class AddAddressToProjectModifications < ActiveRecord::Migration
  def self.up
    change_table :project_modifications do |t|
      t.string   "customer_cep"
      t.string   "customer_address"
      t.string   "customer_address_number"
      t.string   "customer_address_complement"
      t.string   "customer_address_neighborhood"
      t.string   "customer_address_city"
      t.string   "customer_address_state"
    end
  end

  def self.down
    remove_column :project_modifications, :customer_cep
    remove_column :project_modifications, :customer_address
    remove_column :project_modifications, :customer_address_number
    remove_column :project_modifications, :customer_address_complement
    remove_column :project_modifications, :customer_address_neighborhood
    remove_column :project_modifications, :customer_address_city
    remove_column :project_modifications, :customer_address_state
  end
end
