class ModifyProjectModifications < ActiveRecord::Migration
  def self.up
    change_column :project_modifications, :orientation, :string,:limit => 50
  end

  def self.down
    change_column :project_modifications, :orientation, :string, :limit => 10
  end
end