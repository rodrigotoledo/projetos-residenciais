class RemovePriceFromProducts < ActiveRecord::Migration
  def self.up
    remove_column :products, :price
  end

  def self.down
    change_table :products do |t|
      t.float :price
    end
  end
end
