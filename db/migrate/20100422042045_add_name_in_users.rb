class AddNameInUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.string :name
    end
  end

  def self.down
    remove_column :user, :name
  end
end
