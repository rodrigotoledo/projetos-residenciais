// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults
function start_map(latitude,longitude,disabled) {
  var latlng = new google.maps.LatLng(latitude,longitude);
  var myOptions = {
    zoom: 18,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.HYBRID
  };
  var map = new google.maps.Map(document.getElementById("map_canvas"),
      myOptions);
}


$(document).ready(function(){
	$.JMask("#project_modification_cep", "99999999");
	$("#project_modification_cep").blur(function(){
		$.getJSON("/modificar_projeto/endereco",{id:$(this).val()},function(data){
	    $('#project_modification_address').val(data[0]+" "+data[1]);
	    $('#project_modification_neighborhood').val(data[2]);
	    $('#project_modification_city').val(data[4]);
	    $('#project_modification_province').val(data[3]);
	    localize_map();
	  });
	});
	$("#project_modification_house_number").blur(function(){
		localize_map();
	});
});


function localize_map(){
  var info = {
    address:$("#project_modification_address").val(),
    neighborhood:$("#project_modification_neighborhood").val(),
    house_number:$("#project_modification_house_number").val(),
    city:$("#project_modification_city").val(),
    province:$("#project_modification_province").val(),
  };
  $.getJSON("/modificar_projeto/map",info,function(data){
      $("#terreno").show();
      start_map(data.latitude,data.longitude,true);
    
  });
}

function remove_field(element, item) {
  element.up(item).remove();
}

function mycarousel_initCallback(carousel)
{
  // Disable autoscrolling if the user clicks the prev or next button.
  carousel.buttonNext.bind('click', function() {
    carousel.startAuto(0);
  });

  carousel.buttonPrev.bind('click', function() {
    carousel.startAuto(0);
  });

  // Pause autoscrolling if the user moves with the cursor over the clip.
  carousel.clip.hover(function() {
    carousel.stopAuto();
  }, function() {
    carousel.startAuto();
  });
}


function update_quantity(product_value_id,new_quantity){
  $.get("/carrinho/atualizar/"+product_value_id+"/"+new_quantity,function(data){
    $('#product_amount_'+product_value_id).html(data);
    //$('#product_amount_'+product_id).html($('#product_amount_'+product_id).html().maskMoney());
  });
}

function get_endereco(cep){
  $.getJSON("/carrinho/endereco/"+cep,function(data){
    $('#cart_customer_address').val(data[0]+" "+data[1]);
    $('#cart_customer_address_neighborhood').val(data[2]);
    $('#cart_customer_address_city').val(data[4]);
    $('#cart_customer_address_state').val(data[3]);
  });
}


function change_product_value_price(copy_fee_price,product_value_id,price,quantity)
{

  $("#price_"+product_value_id).html(format_currency(parseFloat(price)+(copy_fee_price*quantity)));

  calc_total_product(copy_fee_price);
}


function calc_total_product(copy_fee_price){
  var total = (parseInt($("#product_number_copy").val()) * copy_fee_price) + parseFloat($("#product_price").val());
  $('input.optionals').each( function() { 
    if($(this).attr("checked")){
      total += parseFloat($("#values_"+$(this).val()).val()) + (copy_fee_price * parseInt($("#quantitys_"+$(this).val()).val()));
    }
  });
  $("#amount").html(format_currency(total));
}




function format_currency(num) {
  num = num.toString().replace(/\$|\,/g,'');
  if(isNaN(num))
  num = "0";
  sign = (num == (num = Math.abs(num)));
  num = Math.floor(num*100+0.50000000001);
  cents = num%100;
  num = Math.floor(num/100).toString();
  if(cents<10)
  cents = "0" + cents;
  for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
  num = num.substring(0,num.length-(4*i+3))+'.'+
  num.substring(num.length-(4*i+3));
  return (((sign)?'':'-') + 'R$ ' + num + ',' + cents);
}



$('.add_nested_item').live('click',function(){
    var template = eval($(this).attr('name').replace(/.*#/, ''));
    $("#"+$(this).attr('rel')).append(template);
    $('input[type=submit]').attr('disabled',false);
    $("#"+$(this).attr('rel')).find("tr:last").find("input:first").focus();
});

$('.del_nested_item').live('click',function(){
    $('input[type=submit]').attr('disabled',false);
    var top_element = $(this).parent().parent();
    var flag_destroy = '_destroy';
    if($(this).attr('flag_destroy')){
        flag_destroy = $(this).attr('flag_destroy');
    }
    //Se trata de um objeto existente entao esconde o tr e muda o campo _destroy para true
    if(top_element.find("[name$='["+flag_destroy+"]']").length > 0){
        top_element.find("[name$='["+flag_destroy+"]']").val('true');
        top_element.hide();
    }else{
        top_element.remove();
    }
});