default_run_options[:pty] = true
set :application, "projetos-residenciais"
set :repository,  "https://rtoledo:hahire45@bitbucket.org/rtoledo/#{application}.git"
set :scm, :git

set :user, "projetos_residenciais"
set :password, "YBAPC^hU"
set :use_sudo, false
set :ssh_options, { :forward_agent => true }

set :branch, "master"
set :deploy_via, :remote_cache
set :deploy_to, "/home/#{user}/#{application}"

after 'deploy:update_code', 'deploy:symlink_db'

namespace :deploy do
  desc "Symlinks the database.yml"
  task :symlink_db, :roles => :app do
    [
      'config/database.yml',
      'vendor/gems/be9-acl9-0.11.0',
      'vendor/gems/will_paginate-2.3.14',
      'vendor/gems/authlogic-2.1.5',
      'vendor/gems/geokit-1.5.0'
    ].each do |p|
      run "ln -nfs #{shared_path}/#{p} #{release_path}/#{p}"
    end
  end
end

server "projetosresidenciais.com.br", :app, :web, :db, :primary => true

# if you want to clean up old releases on each deploy uncomment this:
after "deploy:restart", "deploy:cleanup"

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end