require 'smtp-tls'
ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.default_content_type = "text/html"
ActionMailer::Base.default_charset = "utf-8"
ActionMailer::Base.perform_deliveries = true
ActionMailer::Base.smtp_settings = {
  :address => "smtp.gmail.com",
  :port => 587,
  :domain => "gmail.com",
  :authentication => :plain,
  :user_name => "faleconosco@projetosresidenciais.com.br",  # don't put "@gmail.com" here, just your username
  :password => "projetos",
  :enable_starttls_auto => true
}
