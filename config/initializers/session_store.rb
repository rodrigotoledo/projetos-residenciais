# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_sisplan_session',
  :secret      => '74f8945a2747fcc9a74a60c64d7018b17d6e00610869ae7e8a4a074fe78fe9d5561522efaffbf8adad61d0092113a914df44fef77d5dddc996a4d7ea220a2283'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
