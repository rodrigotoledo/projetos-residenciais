namespace :extract_fixtures do
  desc 'Cria o YAML de acordo com o banco'
  task :extract => :environment do
      sql  = "SELECT * FROM %s"
      tables = %w(attributes users categories photos products product_attributes product_values product_types roles roles_users tax_fees copy_fees carts cart_items)
      #tables = %w(linhas estados paises usuarios transportadoras)
      ActiveRecord::Base.establish_connection
      
      
      
      tables.each do |table_name|
        i = "000"
        File.open("#{RAILS_ROOT}/spec/fixtures/#{table_name}.yml", 'w') do |file|
          data = ActiveRecord::Base.connection.select_all(sql % table_name)
          file.write data.inject({}) { |hash, record|
            hash["#{table_name}_#{i.succ!}"] = record
            hash
          }.to_yaml
        end
      end
      
  end
  
  
  task :categories_products => :environment do
      sql  = "SELECT * FROM %s"
      tables = %w(categories_products)
      #tables = %w(linhas estados paises usuarios transportadoras)
      ActiveRecord::Base.establish_connection
      
      
      
      tables.each do |table_name|
        i = "000"
        File.open("#{RAILS_ROOT}/spec/fixtures/#{table_name}.yml", 'w') do |file|
          data = ActiveRecord::Base.connection.select_all(sql % table_name)
          file.write data.inject({}) { |hash, record|
            hash["#{table_name}_#{i.succ!}"] = record
            hash
          }.to_yaml
        end
      end
      
  end
end

